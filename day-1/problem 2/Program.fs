﻿open System.IO

// For more information see https://aka.ms/fsharp-console-apps
let filePath = "input.txt"

let depths (filePath:string) = seq {
    use sr = new StreamReader (filePath)
    while not sr.EndOfStream do
        yield sr.ReadLine ()
}

let triplewise (source: seq<_>) =
    seq { use e = source.GetEnumerator() 
        if e.MoveNext() then
            let i = ref e.Current
            if e.MoveNext() then
                let j = ref e.Current
                while e.MoveNext() do
                    let k = e.Current 
                    yield (!i, !j, k)
                    i := !j
                    j := k }

let normalizedDepths = 
    depths filePath
        |> triplewise
        |> Seq.map (fun (x,y,z) -> int(x) + int(y) + int(z))
        |> Seq.pairwise
        |> Seq.filter (fun (x,y) -> int(y) > int(x))
        |> Seq.length


printf "%A" normalizedDepths
