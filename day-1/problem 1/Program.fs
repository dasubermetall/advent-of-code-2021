﻿open System.IO


// For more information see https://aka.ms/fsharp-console-apps
// printfn "Please input the depths, one by one"

// let depths = fun _ -> Console.ReadLine()
//              |> Seq.initInfinite
//              |> Seq.takeWhile((<>) null)
//              |> Seq.pairwise

let filePath = "input.txt"

let depths (filePath:string) = seq {
    use sr = new StreamReader (filePath)
    while not sr.EndOfStream do
        yield sr.ReadLine ()
}

let count = 
    depths filePath
        |> Seq.pairwise
        |> Seq.filter (fun (x,y) -> int(y) > int(x))
        |> Seq.length


printf "%A" count
